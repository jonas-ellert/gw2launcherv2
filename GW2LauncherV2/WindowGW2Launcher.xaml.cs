﻿using GW2LauncherV2.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GW2LauncherV2
{
    public partial class WindowGW2Launcher : Window
    {
        private const String WELCOME = "Welcome to the Unofficial Guild Wars 2 Launcher!";

        private const int POLLING_RATE = 10;
        private Boolean gw2ExeIsRunning;
        Process gw2Process = null;

        WindowAdvancedSettings advancedSettingsWindow = new WindowAdvancedSettings();

        public WindowGW2Launcher()
        {
            InitializeComponent();
            //this.Icon = Properties.Resources.LauncherLogo;
            printStatus(WELCOME);
            BackgroundThreading.runThread(mainLoopThread);
            BackgroundThreading.runThread(lookForProcessThread);
            btnExitLabel.MouseLeftButtonUp += new MouseButtonEventHandler((sender, e) => App.Current.Shutdown());
            lblDragHandle.MouseLeftButtonDown += new MouseButtonEventHandler((sender, e) => this.DragMove());
            btnLaunch.ButtonClick.Add(new Action(() => requestedAction = LaunchAction.Launch));
            btnImage.ButtonClick.Add(new Action(() => requestedAction = LaunchAction.Image));
            btnDiag.ButtonClick.Add(new Action(() => requestedAction = LaunchAction.Diag));
            btnKillProcess.ButtonClick.Add(new Action(() => killGw2Process()));
            btnAdvanced.ButtonClick.Add(new Action(() =>
            {
                if (advancedSettingsWindow.IsVisible)
                {
                    advancedSettingsWindow.Hide();
                }
                else
                {
                    advancedSettingsWindow.Show();
                    //advancedSettingsWindow.Left = this.Left + (this.Width - advancedSettingsWindow.Width) / 2;
                    //advancedSettingsWindow.Top = this.Top + (this.Height - advancedSettingsWindow.Height) / 2;
                    advancedSettingsWindow.Left = this.Left - advancedSettingsWindow.Width;
                    advancedSettingsWindow.Left = Math.Max(advancedSettingsWindow.Left, 0);
                    advancedSettingsWindow.Top = this.Top;
                }
            }));

            cbxCloseOnLaunch.init(Settings.Default, "CloseOnLaunch");
            cbxQuickstart.init(Settings.Default, "Quickstart");
            if (cbxQuickstart.IsChecked.Value)
            {
                requestedAction = LaunchAction.Launch;
            }

            printStatus("Settings have been loaded.");
        }

        private Boolean lookForProcessThread()
        {
            this.Dispatcher.BeginInvoke(
                new Action(() =>
                {
                    Boolean wasRunning = gw2ExeIsRunning;
                    Process[] candidates = Process.GetProcessesByName("Gw2");
                    gw2ExeIsRunning = (candidates.Length > 0);
                    if (gw2ExeIsRunning)
                    {
                        lblProcessStatus.Foreground = Brushes.Green;
                        lblProcessStatus.Content = "Gw2.exe is running";
                        gw2Process = candidates[0];
                    }
                    else
                    {
                        if (wasRunning) Thread.Sleep(1500);
                        lblProcessStatus.Foreground = Brushes.Red;
                        lblProcessStatus.Content = "Gw2.exe is not running";
                    }
                    btnDiag.isEnabled = btnImage.isEnabled = btnLaunch.isEnabled = !gw2ExeIsRunning;
                    btnKillProcess.isEnabled = gw2ExeIsRunning;
                }));

            Thread.Sleep(POLLING_RATE);
            return true;
        }

        private void killGw2Process()
        {
            printStatus("Trying to terminate GW2 process. . .");
            try
            {
                gw2Process.Kill();
                printStatus("GW2 process successfully terminated.");
            }
            catch (Exception)
            {
                printStatus("Could not terminate GW2 process.");
            }
        }

        private void printStatus(Object o)
        {
            txtStatusBox.printStatus(o);
        }

        private void linkWikiPatchnotes_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }
    }
}
