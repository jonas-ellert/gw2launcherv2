﻿using System;
using System.Threading;
using System.Windows;

namespace GW2LauncherV2
{
    /// <summary>
    /// Static class used to start background threads.
    /// </summary>
    public static class BackgroundThreading
    {
        /// <summary>
        /// Starts a new background thread that repeats the given method until it returns false.
        /// </summary>
        /// <param name="method">Delegate of the method that will be repeatedly executed</param>
        public static void runThread(Func<Boolean> method)
        {
            ParameterizedThreadStart pts = new ParameterizedThreadStart(ThreadWrapper);
            Thread thr = new Thread(pts);
            thr.IsBackground = true;
            thr.Start(method);
        }

        private static void ThreadWrapper(Object innerMethod)
        {
            if (innerMethod is Func<Boolean>)
            {
                Func<Boolean> inMet = (Func<Boolean>)innerMethod;
                while (inMet()) { } 
            }
        }
    }
}
