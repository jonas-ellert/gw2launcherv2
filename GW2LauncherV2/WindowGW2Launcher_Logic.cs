﻿using GW2LauncherV2.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace GW2LauncherV2
{
    public partial class WindowGW2Launcher : Window
    {

        private enum LaunchAction
        {
            Launch,
            Image,
            Diag,
            None
        }
        private LaunchAction requestedAction = LaunchAction.None;

        private Boolean mainLoopThread()
        {
            if (gw2ExeIsRunning && requestedAction != LaunchAction.None)
            {
                printStatus("Action could not be performed: \"Gw2.exe\" already running.");
            }
            if (!gw2ExeIsRunning)
            {
                switch (requestedAction)
                {
                    case LaunchAction.Launch:
                        launchGame();
                        break;
                    case LaunchAction.Image:
                        launchPatch();
                        break;
                    case LaunchAction.Diag:
                        launchDiag();
                        break;
                }
            }
            requestedAction = LaunchAction.None;
            Thread.Sleep(POLLING_RATE);
            return true;
        }

        private void launchGame()
        {
            try
            {
                String argsString = "";
                String argsStringPrint;
                Settings s = Settings.Default;

                //Show dialog before starting repair
                if (s.Repair)
                {
                    MessageBoxResult res = MessageBox.Show(
                                            "Repair may take some minutes. Add \"-repair\" to Arguments?",
                                            "Repair Gw2.dat?",
                                            MessageBoxButton.YesNoCancel,
                                            MessageBoxImage.Question);
                    if (res == MessageBoxResult.Yes)
                    {
                        argsString += "-repair ";
                    }
                    else if (res == MessageBoxResult.Cancel)
                    {
                        return;
                    }
                }

                //Update game
                if (s.Image)
                {
                    launchPatch();
                }

                //Advanced Arguments
                if (s.ClientPort80) argsString += "-clientport " + s.FpsLimit + " ";
                if (s.NoMusic) argsString += "-nomusic ";
                if (s.NoSound) argsString += "-nosound ";
                if (s.NoUI) argsString += "-noui ";
                if (s.PrefReset) argsString += "-prefreset ";
                if (s.UISpan) argsString += "-uispanallmonitors ";
                if (s.OldFov) argsString += "-useoldfov ";

                //Base Arguments
                if (s.Windowed) argsString += "-windowed ";
                if (s.LoadInfo) argsString += "-maploadinfo ";
                if (s.BMP) argsString += "-bmp ";
                if (s.AutoLogin) argsString += "-autologin ";

                argsStringPrint = argsString;
                argsStringPrint += loginManager.getLoginArgsPrint();
                argsString += loginManager.getLoginArgs();


                Process.Start(Settings.Default.ExePath, argsString);
                printStatus("Game started from:\r" + Settings.Default.ExePath + "\rArguments applied:\r" + argsStringPrint);


                //Close application
                if (s.CloseOnLaunch)
                {
                    for (int i = 5; i > 0; i--)
                    {
                        printStatus("Terminating in " + i + " Seconds. . .");
                        Thread.Sleep(1000);
                    }
                    this.Dispatcher.Invoke(new Action(() => Application.Current.Shutdown()));
                }
            }
            catch (Exception e)
            {
                printStatus(getErrorMessage("Launch Game", e));
            }
        }

        private void launchPatch()
        {
            try
            {
                Process.Start(Settings.Default.ExePath, "-image");
                printStatus("Patch started from:\r" + Settings.Default.ExePath + " -image");
                waitForGameToClose();
                printStatus("Patch completed or canceled.");
            }
            catch (Exception e)
            {
                printStatus(getErrorMessage("Start Patch", e));
            }
        }

        private void launchDiag()
        {
            if (
                MessageBox.Show(
                "Diagnosis may take some minutes. Continue?",
                "Create NetworkDiag.log?",
                MessageBoxButton.YesNo,
                MessageBoxImage.Question)
                == MessageBoxResult.Yes)
            {
                try
                {
                    Process.Start(Settings.Default.ExePath, "-diag");
                    printStatus("Diagnostic Process started from:\r" + Settings.Default.ExePath + " -diag");
                    waitForGameToClose();
                    printStatus("Diagnostic Process completed or canceled.");
                }
                catch (Exception e)
                {
                    printStatus(getErrorMessage("Start Diag", e));
                }
            }
        }

        private void waitForGameToClose()
        {
            while (Process.GetProcessesByName("Gw2").Length > 0)
            {
                Thread.Sleep(POLLING_RATE);
            }
            Thread.Sleep(1500);
            if (Process.GetProcessesByName("Gw2").Length > 0) waitForGameToClose();
        }

        private String getErrorMessage(String Operation, Exception e)
        {
            String res = "ERROR: Could not perform \"" + Operation + "\": ";
            if (!File.Exists(Settings.Default.ExePath))
            {
                res += "Game could not be found at \"" + Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Gw2.exe\"";
            }
            else
            {
                res += "An unknown problem occurred (" + e.GetType() + ")";
            }
            return res;
        }
    }
}
