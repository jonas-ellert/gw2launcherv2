﻿using GW2LauncherV2.Properties;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GW2LauncherV2
{
    /// <summary>
    /// Interaktionslogik für WindowAdvancedSettings.xaml
    /// </summary>
    public partial class WindowAdvancedSettings : Window
    {
        public WindowAdvancedSettings()
        {
            InitializeComponent();
            cbxClientport.init(Settings.Default, "ClientPort80");
            cbxNomusic.init(Settings.Default, "NoMusic");
            cbxNosound.init(Settings.Default, "NoSound");
            cbxNoui.init(Settings.Default, "NoUI");
            cbxPrefreset.init(Settings.Default, "PrefReset");
            cbxUispan.init(Settings.Default, "UISpan");
            cbxOldfov.init(Settings.Default, "OldFov");
            cbxRepair.init(Settings.Default, "Repair");

            txtClientPort.Text = Settings.Default.FpsLimit.ToString();
            if (txtClientPort.Text.Equals("0")) txtClientPort.Text = "6112";
            txtClientPort.TextChanged += txtClientPort_TextChanged;

            txtExePath.Text = Settings.Default.ExePath;
            rdbPathDefault.Checked += rdbPathDefault_Checked;
            rdbPathCustom.Checked += rdbPathCustom_Checked;
            rdbPathCustom.IsChecked = Settings.Default.UseCustomPath;
            rdbPathDefault.IsChecked = !Settings.Default.UseCustomPath;
        }

        private void txtFpsLimit_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex reg = new Regex("[0-9]");
            e.Handled = !reg.IsMatch(e.Text);
        }

        private void txtClientPort_TextChanged(object sender, TextChangedEventArgs e)
        {
            Regex reg = new Regex("^[0-9]{0,5}$");
            int start = Math.Max(0, txtClientPort.Text.Length - 5);
            int count = txtClientPort.Text.Length - start;
            txtClientPort.Text = txtClientPort.Text.Substring(start, count);
            if (!reg.IsMatch(txtClientPort.Text))
            {
                txtClientPort.Text = "6112";
            }
            if (txtClientPort.Text.Length > 1 && txtClientPort.Text[0] == '0')
            {
                txtClientPort.Text = txtClientPort.Text.Substring(1, txtClientPort.Text.Length - 1);
            }

            int port = 6112;
            try { port = Convert.ToInt32(txtClientPort.Text); }
            catch (Exception) { }
            Settings.Default.FpsLimit = port;
            Settings.Default.Save();
        }

        private void btnCloseLabel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.Hide();
        }

        private void lblDragHandle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }


        OpenFileDialog ofd = new OpenFileDialog();
        private void btnSelectPath_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ofd.Filter = "Guild Wars 2| Gw2.exe";
            if (ofd.ShowDialog() == true)
            {
                txtExePath.Text = ofd.FileName;
            }
        }

        private void rdbPathDefault_Checked(object sender, RoutedEventArgs e)
        {
            Settings.Default.UseCustomPath = false;
            Settings.Default.Save();
            btnSelectPath.isEnabled = false;

            String path = Assembly.GetEntryAssembly().Location;
            path = System.IO.Path.GetDirectoryName(path) + "\\Gw2.exe";
            txtExePath.Text = path;
        }

        private void rdbPathCustom_Checked(object sender, RoutedEventArgs e)
        {
            Settings.Default.UseCustomPath = true;
            Settings.Default.Save();
            btnSelectPath.isEnabled = true;
            System.Diagnostics.Debug.WriteLine("Custom True");
        }

        private void txtExePath_TextChanged(object sender, TextChangedEventArgs e)
        {
            Settings.Default.ExePath = txtExePath.Text;
            Settings.Default.Save();
        }
    }
}
