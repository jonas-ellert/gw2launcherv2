﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GW2LauncherV2
{
    /// <summary>
    /// Interaktionslogik für ControlGw2StatusBox.xaml
    /// </summary>
    public partial class ControlGw2StatusBox : RichTextBox
    {

        public ControlGw2StatusBox()
        {
            InitializeComponent();
        }

        public void printStatus(Object o)
        {
            this.Dispatcher.BeginInvoke(new Action(() => printStatusInvoked(o)));
        }

        private void printStatusInvoked(Object o)
        {
            this.ScrollToHome();
            TextRange range = new TextRange(this.Document.ContentStart, this.Document.ContentStart);
            range.Text = o.ToString();
            range.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.Black);
            range.ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.Bold);

            range = new TextRange(this.Document.ContentStart, this.Document.ContentStart);
            range.Text = "\r" + DateTime.Now.ToString("HH:mm:ss") + "\r";
            range.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.Red);
            range.ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.Bold);
            this.ScrollToHome();
        }
    }
}
