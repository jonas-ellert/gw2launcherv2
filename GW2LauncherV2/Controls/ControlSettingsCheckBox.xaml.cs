﻿using GW2LauncherV2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GW2LauncherV2
{
    /// <summary>
    /// Interaktionslogik für SettingsCheckbox.xaml
    /// </summary>
    public partial class ControlSettingsCheckBox : CheckBox
    {
        private Settings s;
        private String p;

        public ControlSettingsCheckBox()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Binds the specified settings property to the checkbox.
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="propertyName"></param>
        internal void init(Settings settings, String propertyName)
        {
            if (settings[propertyName] is Boolean)
            {
                s = settings;
                p = propertyName;
                RoutedEventHandler save = new RoutedEventHandler((sender, e) => 
                {
                    s[p] = this.IsChecked.Value;
                    s.Save();
                });
                this.IsChecked = (Boolean)s[p];
                this.Checked += save;
                this.Unchecked += save;
            }
            else
            {
                throw new ArgumentException();
            }
        }
    }
}
