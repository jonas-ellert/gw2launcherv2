﻿using GW2LauncherV2.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GW2LauncherV2
{
    /// <summary>
    /// Interaktionslogik für LoginWindow.xaml
    /// </summary>
    public partial class ControlLoginManager : UserControl
    {

        //TODO: Enter your own random seed
        private const int KEY_MAIL = 0x0;
        private const int KEY_PW = 0x0;
        private readonly Encoding ENCODING = Encoding.UTF8;

        private Boolean validMail = false;
        private Boolean validPw = false;
        private Boolean validLogin = false;

        private struct loginData
        {
            public string mail;
            public string pw;

            public loginData(String emailAddress, String password)
            {
                mail = emailAddress;
                pw = password;
            }
            public Boolean isEmpty()
            {
                return (mail.Equals("") || pw.Equals(""));
            }
        }
        private loginData login = new loginData("", "");

        /// <summary>
        /// Creates a user control that can be used to specify the users Guild Wars 2 account login data
        /// </summary>
        public ControlLoginManager()
        {
            InitializeComponent();
            loadLogin();
            txtMail.Text = login.mail;
            pwPw.Password = login.pw;

            btnClear.ButtonClick.Add(new Action(() => txtMail.Text = pwPw.Password = ""));
            pwPw.PasswordChanged += new System.Windows.RoutedEventHandler(this.pwPw_PasswordChanged);
            txtMail.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txtMail_TextChanged);

            pwPw_PasswordChanged(null, null);
            txtMail_TextChanged(null, null);

            cbxNoPatchUI.init(Settings.Default, "NoPatchUI");
        }

        /// <summary>
        /// Returns the commandline arguments which can be used to directly launch Guild Wars 2 into character selection without showing the official launcher.
        /// </summary>
        /// <returns>Commandline arguments containing the current login data</returns>
        public String getLoginArgs()
        {
            if (validLogin && Settings.Default.NoPatchUI)
            {
                return " -email " + login.mail + " -password " + login.pw + " -nopatchui";
            }
            return "";
        }

        /// <summary>
        /// Returns a print version of the commandline arguments which can be used to directly launch Guild Wars 2 into character selection without showing the official launcher. 
        /// The account password is replaced with "*".
        /// </summary>
        /// <returns>Commandline arguments containing the current login data; account password replaced with "*"</returns>
        public String getLoginArgsPrint()
        {
            if (validLogin && Settings.Default.NoPatchUI)
            {
                return " -email " + login.mail + " -password " + new String('*', login.pw.Length) + " -nopatchui";
            }
            return "";
        }

        /// <summary>
        /// Decrypts the entered login data and saves it in the settings.
        /// </summary>
        private void saveLogin()
        {
            if (validLogin)
            {
                Settings.Default.Encrypted_Mail = encryptString(txtMail.Text, ENCODING, KEY_MAIL);
                Settings.Default.Encrypted_Pw = encryptString(pwPw.Password, ENCODING, KEY_PW);
            }
            else
            {
                Settings.Default.Encrypted_Mail = Settings.Default.Encrypted_Pw = "";
            }
            Settings.Default.Save();
            loadLogin();
        }

        /// <summary>
        /// Loads the encrypted login data which had been saved in the settings before and decrypts it.
        /// </summary>
        private void loadLogin()
        {
            Settings.Default.Reload();
            String mailFromSettings = decryptString(Settings.Default.Encrypted_Mail, ENCODING, KEY_MAIL);
            String pwFromSettings = decryptString(Settings.Default.Encrypted_Pw, ENCODING, KEY_PW);
            validLogin = mailIsValid(mailFromSettings) && pwIsValid(pwFromSettings);
            if (validLogin)
            {
                login = new loginData(mailFromSettings, pwFromSettings);
            }
            else
            {
                login = new loginData("", "");
            }
            cbxNoPatchUI.IsEnabled = validLogin;
        }

        private String encryptString(String data, Encoding enc, int key)
        {
            byte[] dataBytes = enc.GetBytes(data);
            crypto(dataBytes, key);
            return Convert.ToBase64String(dataBytes);
        }

        private String decryptString(String data, Encoding enc, int key)
        {
            byte[] dataBytes = Convert.FromBase64String(data);
            crypto(dataBytes, key);
            return enc.GetString(dataBytes);
        }

        /// <summary>
        /// Uses the specified key to apply symmetric en-/decryption to the given data.
        /// </summary>
        /// <param name="data">Data to en-/decrypt</param>
        /// <param name="rng">Random number generator</param>
        private void crypto(byte[] data, int key)
        {
            Random rng = new Random(key);
            byte[] dataRnd = new byte[data.Length];
            rng.NextBytes(dataRnd);
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = Convert.ToByte(data[i] ^ dataRnd[i]);
            }
        }

        private void txtMail_TextChanged(object sender, TextChangedEventArgs e)
        {
            validMail = mailIsValid(txtMail.Text);
            validLogin = validMail && validPw;
            if (!validMail)
            {
                lblMailStatus.Content = "Please enter a valid email address.";
                txtMail.BorderBrush = lblMailStatus.Foreground = Brushes.Red;
            }
            else
            {
                lblMailStatus.Content = "Valid.";
                txtMail.BorderBrush = lblMailStatus.Foreground = Brushes.Green;
            }
            saveLogin();
        }

        private void pwPw_PasswordChanged(object sender, RoutedEventArgs e)
        {
            validPw = pwIsValid(pwPw.Password);
            validLogin = validMail && validPw;
            if (!validPw)
            {
                lblPwStatus.Content = "At least 8 characters required.";
                pwPw.BorderBrush = lblPwStatus.Foreground = Brushes.Red;
            }
            else
            {
                lblPwStatus.Content = "Valid.";
                pwPw.BorderBrush = lblPwStatus.Foreground = Brushes.Green;
            }
            saveLogin();
        }

        private Boolean mailIsValid(String m)
        {
            return Regex.IsMatch(m, @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");
        }

        private Boolean pwIsValid(String p)
        {
            return p.Length >= 8;
        }
    }
}
