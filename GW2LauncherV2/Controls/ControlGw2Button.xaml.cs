﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GW2LauncherV2
{
    /// <summary>
    /// Interaktionslogik für Gw2Btn.xaml
    /// </summary>
    /// 
    [DefaultEvent("MouseLeftButtonUp")]
    public partial class ControlGw2Button : Label
    {
        private Boolean enabled = true;

        public Boolean isEnabled
        {
            get
            {
                return enabled;
            }
            set 
            {
                if (!value && enabled)
                {
                    this.Foreground = disabledFG;
                    this.Background = disabledBG;
                }
                else if (value && !enabled)
                {
                    this.Foreground = enabledFG;
                    this.Background = enabledBG;
                }
                enabled = value;
            }
        }

        private LinearGradientBrush enabledBG = new LinearGradientBrush();
        private LinearGradientBrush pressedBG = new LinearGradientBrush();
        private LinearGradientBrush hoverBG = new LinearGradientBrush();
        private LinearGradientBrush disabledBG = new LinearGradientBrush();

        private Brush enabledFG = Brushes.White;
        private Brush pressedFG = Brushes.White;
        private Brush hoverFG = Brushes.White;
        private Brush disabledFG = Brushes.LightGray;

        public List<Action> ButtonClick = new List<Action>();

        public ControlGw2Button()
        {
            InitializeComponent();
            enabledBG.StartPoint = new Point(0.5, 0);
            enabledBG.EndPoint = new Point(0.5, 1);
            enabledBG.GradientStops.Add(new GradientStop(Color.FromRgb(0x7D, 0x00, 0x00), 0));
            enabledBG.GradientStops.Add(new GradientStop(Color.FromRgb(0xE1, 0x00, 0x00), 1));

            disabledBG.StartPoint = new Point(0.5, 0);
            disabledBG.EndPoint = new Point(0.5, 1);
            disabledBG.GradientStops.Add(new GradientStop(Colors.DimGray, 0));
            disabledBG.GradientStops.Add(new GradientStop(Colors.Gray, 1));

            pressedBG.StartPoint = new Point(0.5, 0);
            pressedBG.EndPoint = new Point(0.5, 1);
            pressedBG.GradientStops.Add(new GradientStop(Color.FromRgb(0x26, 0x26, 0x26), 0));
            pressedBG.GradientStops.Add(new GradientStop(Color.FromRgb(0x44, 0x44, 0x44), 0.5));
            pressedBG.GradientStops.Add(new GradientStop(Color.FromRgb(0xFF, 0xFF, 0xFF), 1));

            hoverBG.StartPoint = new Point(0.5, 0);
            hoverBG.EndPoint = new Point(0.5, 1);
            hoverBG.GradientStops.Add(new GradientStop(Color.FromRgb(0x7D, 0x00, 0x00), 0));
            hoverBG.GradientStops.Add(new GradientStop(Color.FromRgb(0xE1, 0x00, 0x00), 0.5));
            hoverBG.GradientStops.Add(new GradientStop(Color.FromRgb(0xFF, 0xFF, 0xFF), 1));

            this.MouseLeftButtonDown += (sender, e) => onPress();
            this.MouseLeftButtonUp += (sender, e) => onRelease();
            this.MouseEnter += (sender, e) => onHover();
            this.MouseLeave += (sender, e) => onLeave();
        }

        private Boolean checkEnabled()
        {
            if (enabled) return true;
            else
            {
                this.Foreground = disabledFG;
                this.Background = disabledBG;
                return false;
            }
        }

        private void onPress()
        {
            if (!checkEnabled()) return;
            this.Background = pressedBG;
            this.Foreground = pressedFG;
            checkEnabled();
        }

        private void onRelease()
        {
            if (!checkEnabled()) return;
            foreach (Action action in ButtonClick)
            {
                action();
            }
            onLeave();
            checkEnabled();
        }

        private void onLeave()
        {
            if (!checkEnabled()) return;
            this.Background = enabledBG;
            this.Foreground = enabledFG;
            this.FontWeight = FontWeights.Regular;
            if (this.IsMouseOver)
            {
                this.onHover();
            }
            checkEnabled();
        }

        private void onHover()
        {
            if (!checkEnabled()) return;
            this.Background = hoverBG;
            this.Foreground = hoverFG;
            this.FontWeight = FontWeights.Bold;
            checkEnabled();
        }
    }
}
