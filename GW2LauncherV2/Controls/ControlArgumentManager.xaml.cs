﻿using GW2LauncherV2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GW2LauncherV2
{
    /// <summary>
    /// Interaktionslogik für ControlArgumentManager.xaml
    /// </summary>
    public partial class ControlArgumentManager : UserControl
    {
        public ControlArgumentManager()
        {
            InitializeComponent();
            cbxImage.init(Settings.Default, "Image");
            cbxLoadInfo.init(Settings.Default, "LoadInfo");
            cbxBmp.init(Settings.Default, "BMP");
            cbxWindowed.init(Settings.Default, "Windowed");
            cbxAutoLogin.init(Settings.Default, "AutoLogin");
        }
    }
}
